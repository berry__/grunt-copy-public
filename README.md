# grunt-copy-public

Copy assets from Bower component directories to a flattened structure in a public dir.
This should make for a neat public folder with sensible paths.


```shell
npm install grunt-copy-public --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-copy-public');
```

## The "copy_public" task

### Overview
In your project's Gruntfile, add a section named `copy_public` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
    copy_public: {
      dist: {
        src: '_BOWERDIR_/**/public/**',
        // Specifies which directory to use inside the components
        // _BOWERDIR_ will be replaced with the directory element inside your .bowerrc file
        // or default to 'bower_components'
        
        dest: 'public'
        // Target base directory
        // Files will be stored in a subdirectory structure similar to the one they are found in
        // combined with the components name
      },

      options: {
        useComponentName: true
        // Whether to use the component name in the target path (eg. /public/fonts/component-name/filename.ext)
        // Defaults to true.
      }
    },
});
```

